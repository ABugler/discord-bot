/* Libraries dont have books anymore! */
const Discord = require("discord.js");
const { promisify } = require("util"); // util.inspect( object, showHidden?, depth/null )
const readDir = promisify(require("fs").readdir); // Short for FileSystem :D

/* Define the bot (Discord calls it a client) */
const bot = new Discord.Client();
/* Load those swweeett configurations */
bot.config = require("./botconfig.json");
/* Extend the functionality of the bot with our own functions */
require("./modules/botFunctions.js")(bot);

/* Bring all our events and commands into the app - once we have all of them! */
const initBotAsync = async () => {
bot.log("initializing bot");

    bot.log("loading commands");
    /* Load up our commands */
    const cmds = await readDir("./commands/");
    bot.log(`Loading all ${cmds.length} of our commands.`);
    cmds.forEach( file => {
      /* Should NEVER have anything other than .js command files in the commands folder....
          but just in case of a fire */
      if (!file.endsWith(".js")) return;
      const response = bot.loadCommand(file);
    });

    /* Here we're loading up our events */
    const events = await readDir("./events/");
    bot.log(`Loading all ${events.length} of our events.`);
    events.forEach( f => {
      const eventName = f.split(".")[0];
      const event = require(`./events/${f}`);
      /* BOO YAA. Bot binds events & event triggers to itself */
      bot.on(eventName, event.bind(null, bot));
      delete require.cache[require.resolve(`./events/${f}`)];
    });

    /* Log the bot in with all the functionality above! */
    bot.login(bot.config.token);
    bot.log("Bot initializiation successful");
};

initBotAsync();
