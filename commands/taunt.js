/* WOLOLOOO */

exports.run = async (bot, message, taunts) => {
  /* Voice only works over guilds */
    if(!message.guild) return;

  /* Saying the message over the channel the sender is on */
    if ( message.member.voiceChannel) {
      this.sequentialTaunts(bot, message, taunts);
    }
    else {
      message.reply(`HEY ${message.member.displayName}! Omg join a voice channel fist :\\ `);
    }
};

exports.sequentialTaunts = async function (bot, message, taunts) {
  try
  {
    const vc = message.member.voiceChannel;
    /* await is an async keyword that stops other node events until it returns */
    const vcc = await vc.join(); /* a.k.a. 'await' guarantees we join the channel */

    for (const taunt of taunts) {
    /* awaiting the end of the current taunt before going to the next */
      await this.playTaunt(bot, vcc, taunt);
    }

  } catch (error) { bot.log(`sequentialTaunts error: ${error}`); }
};

exports.playTaunt = async function (bot, connection, taunt) {
  try
  {
    const dispatch = await connection.playFile(`./assets/sounds/aoeTaunts/${taunt}.mp4`, {
      volume: 0.75,
      passes: 2
    });

  /* discord voice events aren't very trustworthy, but better safe than sorry */
    dispatch.on("error", function(e) {
      bot.log(`Taunt error - ${e}`);
    });

  /* Huge race condition... But not really sure how else to guarantee the audio playback is finished */
    while ( !dispatch.destroyed ) await bot.wait(50);

    bot.log(`dispatch over & out! Total time: ${dispatch.totalStreamTime} Speak time: ${dispatch.time} `);
    dispatch.end();

  } catch (error) { bot.log(`playTaunt error: ${error}`); }
};

exports.help = {
    name: "taunt",
    category: "Social",
    description: "Troll 'em. Troll 'em all.",
    usage: "[.aoe <taunt #>]",
    aliases: ["taunt", "aoe"]
};
