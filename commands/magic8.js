/* Little magic8 ball - so we can finally, finally know */

const answers = ["Maybe.", "Certainly not.", "I hope so.", "Not in your wildest dreams.", "There is a good chance.", "Quite likely.", "I think so.", "I hope not.", "I hope so.", "Never!", "Fuhgeddaboudit.", "Ahaha! Really?!?", "Pfft.", "Sorry, bucko.", "Hell, yes.", "Hell to the no.", "The future is bleak.", "The future is uncertain.", "I would rather not say.", "Who cares?", "Possibly.", "Never, ever, ever.", "There is a small chance.", "Yes!"];


exports.run = async (bot, message) => {
  /* making sure they're asking a question... */
  if (message.content.indexOf("?") === message.content.length - 1) {
    /* Reply will automagically tag the message author in its message */
    message.reply(`:8ball: says: ${answers.random()}`).catch(err => bot.log(err));
  } else {
    /* No question = sass */
    message.reply(":8ball: That doesn't look like a question :\ try again please").catch(err => bot.log(err));
  }
};

exports.help = {
  name: "magic8",
  category: "Social",
  description: "ask away... if you dare!",
  usage: "[.magic8 was this a good idea?]",
  aliases: ["magic8", "m8", ":8ball:", "magic8ball"]
};
