/* Prints some random server/guild info */

exports.run = async ( bot, message, args, ...rest ) => {
    let richInfo = bot.richMsg()
      .setDescription("Server Info")
      .setColor("#15f153")
      .setThumbnail(message.guild.displayAvatarURL)
      .addField("Server Name", message.guild.name)
      .addField("Server BDay", message.guild.createdAt)
      .addField(`${message.member.displayName} Joined `, `${message.member.joinedAt}`)
      .addField("Guild Size", message.guild.memberCount);

    message.channel.send(richInfo);
};

exports.help = {
  name: "serverinfo",
  category: "Info",
  description: "little info about the current Discord server",
  usage: "[.serverinfo]",
  aliases: ["sinfo", "si", "serverinfo"]
};
