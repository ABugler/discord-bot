/* Basic greeting the bot will give back...  TODO: refactor into an on("join") type of message? */

exports.run = async ( bot, message, args, ...rest ) => {
    let response = `Hello there ${message.author.username}!`;

    if ( [true, false].random() ) {
      response += (message.author.presence.game !== null )
          ? `\nhaving fun with ${message.author.presence.game.name}?`
          : `\nlet's play some vids :video_game:`;
    }

    message.channel.send(response);
};

exports.help = {
  name: "hello",
  category: "Social",
  description: "Greetings - from the bot",
  usage: "[.hello]",
  aliases: ["hi", "hey", "hello"]
};
