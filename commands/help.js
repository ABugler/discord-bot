/* Give a man a function and he'll eat.
 * Give a man good documentation and he'll never go hungry */

exports.run = async (bot, message, args, ...rest) => {
    bot.log(`in the help cmd ${message.content}, ${args}`);

    let help = bot.richMsg()
      .setColor("#33f7f0")
      .setThumbnail(bot.user.displayAvatarURL);

    /* No command specified.... Give 'em all we got! */
    if (!args[0]) {
      /* lil PSA on how to get moar info */
      help.setTitle(`Command List --- Use "${bot.config.prefix}help <commandName>" for details!`);

      /* Gotta know when to hold 'em */
      const cmds = bot.commands.filter(cmd => cmd.help.category !== "Secret");

      /* Keepin' it clean */
      const sortedCmds = cmds.array().sort( (c1, c2) =>
        c1.help.category > c2.help.category ? 1 : c1.help.name > c2.help.name && c1.help.category === c2.help.category ? 1 : -1 );

      /* Not a very pretty embed but..... Yeah its ugly */
      let category = "";
      sortedCmds.forEach( cmd => {
        if ( category !== cmd.help.category ) {
          category = cmd.help.category;
          help.addField(`${cmd.help.category}`, "commands:");
        }
        help.addField(`[${cmd.help.name}]`, cmd.help.description);
      });

    } else {
      /* the command they want info on */
      let cmd = args[0];
      if (bot.aliases.has(cmd)) {
        cmd = bot.commands.get(bot.aliases.get(cmd));
        help.setTitle(`The ${cmd.help.name} command`);
        help.addField("Name", cmd.help.name);
        help.addField("Category", cmd.help.category);
        help.addField(`Description`, cmd.help.description);
        help.addField("Usage", cmd.help.usage);
        help.addField("Aliases", cmd.help.aliases.join(`\n`));
      } else {
        help.setDescription(`Nothing like ${cmd} has been coded!`);
      }
    }

    message.channel.send(help);
};

exports.help = {
  name: "help",
  category: "Info",
  description: "It helps. Sometimes.",
  usage: "[.help <command name>] (or just [.help])",
  aliases: ["help", "-h", "wtf"]
};
