/* Silly command to search urban dictionary for definitions
      ....and practice before getting google results */

const request = require("request");

exports.run = async (bot, message, args) => {

  /* Checking if they passed a maxResult number.... Defaulting to 1 */
  let maxResults = !isNaN(args.slice(-1)) ? args.pop() : 1;
  let queryStr = args.join("+");

  const baseUrl = "http://api.urbandictionary.com/v0/define?term=";
  const searchUrl = baseUrl + queryStr;

  request( {
      url: searchUrl,
      json: true
    }, ( error, response, body ) => {
      /* I think this will be enough of a check... */
      if ( body.list === null || body.list.length === 0 ) {
        message.channel.send(`No definition for ${args.join(" ")} was found`);
      } else {
        /* dont wanna take the OB strokes */
        maxResults = maxResults > body.list.length ? body.list.length : maxResults;
        /* lets see what crazy things come back */
        for ( let i = 0; i <= maxResults - 1; i++) {
          let result = body.list[i];
          /* Still dont know markdown formatting well... But discord will format it! */
          let urbanDefinition = [
            `**Searched:** ${args.join(" ")}`,
            "",
            `**Definition:** ${i + 1} out of ${body.list.length}\n_${result.definition}_`,
            `**Example:**\n${result.example}`,
            `<${result.permalink}>`,
          ];
          message.channel.send(urbanDefinition).catch(err => bot.log(err));
        }
      }
    });
};

exports.help = {
    name: "urban",
    category: "Social",
    description: "clarify a term (via urban dictionary)",
    usage: "[.urban {term/phrase to search} <number of results>]",
    aliases: ["urban", "u", "lookup", "clarify"]
};
