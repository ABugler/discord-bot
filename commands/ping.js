/* Calculates ping between sending a message and seeing it (rount-trip)
      aaaannd gets the server ping */

exports.run = async ( bot, message, args, ...rest) => {
    bot.log("   ...starting ping");
    const msg = await message.channel.send("ping?");

    let bjiv = bot.user.displayAvatarURL;
    let richMsg = bot.richMsg()
      .setDescription("Pong!")
      .setColor("#15f153")
      .setThumbnail(bjiv)
      .addField("Total Time:", `${msg.createdTimestamp - message.createdTimestamp}ms`)
      .addField("Server Ping:", `${bot.ping}ms`);

    msg.edit(richMsg);
};

exports.help = {
    name: "ping",
    category: "Info",
    description: "pings and pongs. Not a game of Ping-Pong",
    usage: "[.ping]",
    aliases: ["ping"]
};
