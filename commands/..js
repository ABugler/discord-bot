/* Sneaky ghost command to make it seem like the bot is talking >:D */

exports.run = async ( bot, message, args, ...rest ) => {
    const botSpeak = args.join(" ");

    message.delete().catch( O_O => {} );

    message.channel.send(botSpeak);
};

exports.help = {
  name: "Bot Whisper",
  category: "Secret",
  description: "Make the bot say whatever you want",
  usage: "[. stuff i want the bot to say]",
  aliases: ["speak", "whisper", "."]
};
