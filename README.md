# Discord-Bot

Setting up a place to work on a discord bot!

## Requirements
To get going you're gonna need:
    *git
    *node (at least Version 8.0.0 )

## Setup
Then once you have those just:

1. Create the folder you'll be storing the project in (lets call it 'Discord-Bot'
2. get into that folder [`cd Discord-Bot`]
3. clone the repo into your empty folder [`git clone https://bitbucket.org/ABugler/discord-bot.git .`]
4. Done! Test it out by running [`node app.js`] from the root folder

## Starting up the bot
To actually run the bot, run `node app.js` from the root folder (Discord-Bot)


## Adding Commands
Even at 4 the command parsing block was too big and ugly... Now there's command & event pipeline! :satisfied: !

Just add your new command/event as a node module in its respective `./commands`/`./events` folder.

## Commands
Put the command logic in a `exports.run = async () => { your code here; };` block

Put some info about the command into json `exports.help` property

### Events
Pretty much the same deal with events...

Put the event logic in a `module.exports = () => { code heeeeerrrrr; };` block

*BUT*

The way its written, the event file name must exactly match the name of the event being fired

(i.e. bot.on("someEvent", () =>{}); put the logic in ./events/*someEvent*.js)
