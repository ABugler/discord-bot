/* Styling for the logger */
const chalk = require("chalk");
/* Time/Clock library */
const moment = require("moment");
/* Discord lib for the collections */
const Discord = require("discord.js");

module.exports = (bot) => {
/* ez logger method */
  bot.log = (...args) => console.log(...args);

  bot.log("...Enhancing bot");
/* Collections for all of our commands/aliases, and events */
  bot.commands = new Discord.Collection();
  bot.aliases = new Discord.Collection();
  bot.events = new Discord.Collection();

/* Main Helper method for the command engine */
  bot.loadCommand = (commandName) => {
      try {
        const cmd = require(`../commands/${commandName}`);
        bot.log(`Loading Command: ${cmd.help.name}.`);

        bot.commands.set(cmd.help.name, cmd);
        cmd.help.aliases.forEach( alias => {
          bot.aliases.set(alias, cmd.help.name);
        });

        bot.log(` ...loaded: ${commandName}`);
        return false;
      }
      catch (error) {
        return `Unable to load command ${commandName}: ${error}`;
      }
  };

  /* Global pretty printing */
  bot.richMsg = function () { return new Discord.RichEmbed(); };

/* Fake randomness functions */
  Array.prototype.random = function() {
  /* Gets a random element of the array */
      return this[Math.floor(Math.random() * this.length)]
  };

/* Fake call-stack for any errors I havent tried/caught on my own */
  bot.logError = (error) => {
    const errorMsg = error.stack.replace(new RegExp(`${__dirname}/`, "g"), "./");
    bot.log(`Uncaught Exception: ${errorMsg}`);

    /* kill the bot, and go fix the error! */
    process.exit(1);
  };
  process.on("uncaughtException", (error) => {
    bot.logError(error);
  });
  process.on("referenceError", (error) => {
    bot.logError(error);
  });

/* Making the logger part of the bot (and accessible errywhere)
  bot.log = (content, level = "log") => {
      const timestamp = `[${moment().format("MM-DD-YYYY HH:mm:ss")}]:`;
      switch ( level ) {
        case "log": {
          return console.log(`${timestamp} ${chalk.bgBlue(type.toUpperCase())} ${content} `);
        }
        case "warn": {
          return console.log(`${timestamp} ${chalk.black.bgYellow(type.toUpperCase())} ${content} `);
        }
        case "error": {
          return console.log(`${timestamp} ${chalk.bgRed(type.toUpperCase())} ${content} `);
        }
        case "debug": {
          return console.log(`${timestamp} ${chalk.green(type.toUpperCase())} ${content} `);
        }
        case "cmd": {
          return console.log(`${timestamp} ${content}`);
        }
        case "ready": {
          return console.log(`${timestamp} ${chalk.black.bgGreen(type.toUpperCase())} ${content}`);
        }
        default: throw new TypeError("Logger type invalid. Use log, warn, error, debug, cmd, or ready");
      }
  };
 cant get chalk to work correctly : ( */

  /* wait function, in case needed in multiple spaces */
  bot.wait = require("util").promisify(setTimeout);

  bot.log("Bot is all enhanced!");
};
