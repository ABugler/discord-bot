/* the ready file... kinda like on start
    its the first thing that happens (i think) OK?!? */

module.exports = async bot => {
  /* Sometimes everythings not there? Have to pull onto my desktop and retest */
  await bot.wait(1000);

  /* Things to do once the bot comes online */
  bot.log(`[READY] ${bot.user.username} just hopped on. Serving ${bot.users.size} across ${bot.guilds.size} servers`);

  bot.user.setPresence( {
      game: {
        name: "with the code"
      },
      status: "playing"
  });

  bot.log(`${bot.user.username} is ${bot.user.presence.status} ${bot.user.presence.game.name}`);
};
