/* Message engine for the bots brain (runs on every message ever/anywhere) */

module.exports = (bot, message) => {
  /* Ignoring other bots - also makes your bot ignore itself ( and not get stuck in bot loop) */
  if(message.author.bot) return;

  /* changed my mind! tag the bot to interact */
  let botTag = `<@${bot.user.id}>`;
  let match = new RegExp(`^${botTag}`);
  let prefix = bot.config.prefix.length;

  /* Only want the bot to react to our special command prefix (set in the config!) (or after being tagged!) */
  if(message.content.indexOf(bot.config.prefix) !== 0) {
    if (!match.test(message.content)) return;
    prefix = botTag.length;
  }
  /* im also ignoring direct messages for now.... might make it an admin-y thing later... */
  if(message.channel.type === "dm") return;

  bot.log(`  Incoming message: ${message.content}`);

  /* Break up the prefix, command and potential params for the command  (shift removes the 1st e in the arr)*/
  const params = message.content.slice( prefix ).trim().split(/ +/g);
  const command = params.shift().toLowerCase();

  /* Look for the command by name/alias and pull it out of the cmds collection */
  const cmd = bot.commands.get(command) || bot.commands.get(bot.aliases.get(command));

  /* If we didnt find the command.... there's nothing to do */
  if (!cmd) { bot.log(`could not find ${cmd}.`); return; }

  /* Otherwise [log it and] run the command! */
  cmd.run(bot, message, params);
  bot.log(`[COMMAND] ${message.author.username} ran commmand ${cmd.help.name}`);
};
